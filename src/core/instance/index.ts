import { initMixin } from './init'
import { stateMixin } from './state'
import { renderMixin } from './render'
import { eventsMixin } from './events'
import { lifecycleMixin } from './lifecycle'
import { warn } from '../util/index'
import type { GlobalAPI } from 'types/global-api'

// Vue 构造函数
function Vue(options) {
  if (__DEV__ && !(this instanceof Vue)) {
    warn('Vue is a constructor and should be called with the `new` keyword')
  }
  // 调用 Vue.prototype._init 方法，该方法是在 initMixin 中定义的
  this._init(options)
}

//@ts-expect-error Vue has function type
initMixin(Vue) // initMixin方法主要实现了_init方法。 选项的合并，数据初始化（如响应式处理），以及触发编译和渲染的流程
//@ts-expect-error Vue has function type
stateMixin(Vue) // 主要实现了data,props的代理功能
//@ts-expect-error Vue has function type
eventsMixin(Vue)
//@ts-expect-error Vue has function type
lifecycleMixin(Vue) // 实现了三个方法：_update方法非常重要，它主要负责将vnode生成真实节点。
//@ts-expect-error Vue has function type
renderMixin(Vue) //

export default Vue as unknown as GlobalAPI
